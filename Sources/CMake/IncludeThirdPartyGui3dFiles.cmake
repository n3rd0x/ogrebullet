# ************************************************************
# Project:  OgreBullet (https://bitbucket.org/alexeyknyshev/ogrebullet.git)
# Author:   T.Sang Tran
# ************************************************************




# ************************************************************
# Configurations
# ************************************************************
# REF: https://github.com/Valentin33/Gui3D
set(THIRD_PARTY_GUI3D_PATH_ROOT "${PROJECT_PATH_ROOT}/Third-Parties/Gui3D")
set(THIRD_PARTY_GUI3D_PATH_HEADER "${THIRD_PARTY_GUI3D_PATH_ROOT}/build/include")
set(THIRD_PARTY_GUI3D_PATH_SOURCE "${THIRD_PARTY_GUI3D_PATH_ROOT}/build/src")
set(THIRD_PARTY_GUI3D_INCLUDE_DIR
    "${THIRD_PARTY_GUI3D_PATH_HEADER}"
)



# ************************************************************
# Header Files
# ************************************************************
file(GLOB_RECURSE THIRD_PARTY_GUI3D_HEADER_FILES "${THIRD_PARTY_GUI3D_PATH_HEADER}/Gui3D/*.h")




# ************************************************************
# Source Files
# ************************************************************
file(GLOB_RECURSE THIRD_PARTY_GUI3D_SOURCE_FILES "${THIRD_PARTY_GUI3D_PATH_SOURCE}/*.cpp")




# ************************************************************
# Macro
# ************************************************************
# Set header and source files.
macro(ENABLE_THIRD_PARTY_GUI3D_FILES)
    # Group files.
    source_group("Header Files\\Third-Parties\\Gui3D" FILES ${THIRD_PARTY_GUI3D_HEADER_FILES})
    source_group("Source Files\\Third-Parties\\Gui3D" FILES ${THIRD_PARTY_GUI3D_SOURCE_FILES})

    # Add into global header file variable.
    set(LOCAL_GROUP_HEADER_FILES ${LOCAL_GROUP_HEADER_FILES} ${THIRD_PARTY_GUI3D_HEADER_FILES})
    set(LOCAL_GROUP_SOURCE_FILES ${LOCAL_GROUP_SOURCE_FILES} ${THIRD_PARTY_GUI3D_SOURCE_FILES})
    
    # Set include directory
    include_directories(${THIRD_PARTY_GUI3D_INCLUDE_DIR})
endmacro()


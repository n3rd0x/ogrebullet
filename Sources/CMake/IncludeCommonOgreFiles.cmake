# ************************************************************
# Project:  OgreBullet (https://bitbucket.org/alexeyknyshev/ogrebullet.git)
# Author:   T.Sang Tran
# ************************************************************




# ************************************************************
# Configurations
# ************************************************************
set(LOCAL_COMMON_OGRE_PATH_ROOT "${PROJECT_PATH_SOURCE}/Common")
set(LOCAL_COMMON_OGRE_PATH_HEADER "${LOCAL_COMMON_OGRE_PATH_ROOT}/include")
set(LOCAL_COMMON_OGRE_PATH_SOURCE "${LOCAL_COMMON_OGRE_PATH_ROOT}/src")
set(LOCAL_COMMON_OGRE_INCLUDE_DIR
    "${LOCAL_COMMON_OGRE_PATH_HEADER}"
)



# ************************************************************
# Header Files
# ************************************************************
set(LOCAL_COMMON_OGRE_HEADER_FILES
    "${LOCAL_COMMON_OGRE_PATH_HEADER}/ExampleApplication.h"
    #"${LOCAL_COMMON_OGRE_PATH_HEADER}/ExampleFrameListener.h"
    "${LOCAL_COMMON_OGRE_PATH_HEADER}/OgreBulletApplication.h"
    "${LOCAL_COMMON_OGRE_PATH_HEADER}/OgreBulletGuiListener.h"
    "${LOCAL_COMMON_OGRE_PATH_HEADER}/OgreBulletInputListener.h"
    "${LOCAL_COMMON_OGRE_PATH_HEADER}/OgreBulletListener.h"
)




# ************************************************************
# Source Files
# ************************************************************
set(LOCAL_COMMON_OGRE_SOURCE_FILES
    "${LOCAL_COMMON_OGRE_PATH_SOURCE}/OgreBulletApplication.cpp"
    "${LOCAL_COMMON_OGRE_PATH_SOURCE}/OgreBulletGuiListener.cpp"
    "${LOCAL_COMMON_OGRE_PATH_SOURCE}/OgreBulletInputListener.cpp"
    "${LOCAL_COMMON_OGRE_PATH_SOURCE}/OgreBulletListener.cpp"
)




# ************************************************************
# Macro
# ************************************************************
# Set header and source files.
macro(ENABLE_LOCAL_COMMON_OGRE_FILES)
    # Group files.
    source_group("Header Files\\Common" FILES ${LOCAL_COMMON_OGRE_HEADER_FILES})
    source_group("Source Files\\Common" FILES ${LOCAL_COMMON_OGRE_SOURCE_FILES})

    # Add into global header file variable.
    set(LOCAL_GROUP_HEADER_FILES ${LOCAL_GROUP_HEADER_FILES} ${LOCAL_COMMON_OGRE_HEADER_FILES})
    set(LOCAL_GROUP_SOURCE_FILES ${LOCAL_GROUP_SOURCE_FILES} ${LOCAL_COMMON_OGRE_SOURCE_FILES})
    
    # Set include directory
    include_directories(${LOCAL_COMMON_OGRE_INCLUDE_DIR})
endmacro()

